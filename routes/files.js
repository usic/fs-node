var config = require('../config.json'),
	FileModel = require('../models/file.js'),
	mongo = require('mongodb'),
	Grid = require("gridfs-stream"),
	html_strip = require('htmlstrip-native'),
	path = require('path');

var stripOptions = {
        include_script : true,
        include_style : true,
        compact_whitespace : true
    };

var allowedMime = ["application/pdf"];

function FileHandler(db){

	var fileModel = new FileModel(db);
	var gfs = new Grid(db, mongo);

	this.displayUpload = function(req, res, next){

		//TODO Add user`s name render if logged in
		
		return res.sendfile(path.resolve('views/landing.html'));
	}

	this.handleUpload = function(req, res, next){

		req.pipe(req.busboy);

		req.busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {

			if(allowedMime.indexOf(mimetype) == -1) {

				res.send(400, {success: false, error: 'Bad mime type'});
				file.resume();

			} else {

		    	var store = gfs.createWriteStream({mode: 'w', filename: filename, content_type: mimetype, root: 'gridFS'});
		    	file.pipe(store);
		    	store.on('close', function(file) {
		    		res.send({success: true, fileID: file._id});
		    		res.end();
		    	});

		    	store.on('error', function(err) {
		    		return next(err);
		    	});
	    	}
  		});

	}

	this.handleInfo = function(req, res, next){

		if (!req.body.name || !req.body.tags || !req.body.files) {
			return res.send(400, {success: false, error: "Not all parameters"});
		};

		var name = html_strip.html_strip(req.body.name, stripOptions);
		var tags = html_strip.html_strip(req.body.tags, stripOptions).split(',');
		var files = html_strip.html_strip(req.body.files, stripOptions).split(',');

		//Change when auth
		var user = "guest";

		fileModel.insertFile(name, user, tags, files, function(err, file){

			if(err) return next(err);

			return res.send(200, {success: true});
		});
	}

	
}

module.exports = FileHandler;