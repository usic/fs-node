exports.errorHandler = function(err, req, res, next) {
    console.error("["+new Date()+"] "+err.message);
    console.error(err.stack);
    res.send(500, {success: false, error: "Stay still and don`t move. An error occurred!!!"})
}