var FileHandler = require('./files')
  , ErrorHandler = require('./error').errorHandler;

module.exports = function(app, db) {

    var fileHandler = new FileHandler(db);

    //Main page
    app.get('/', fileHandler.displayUpload);

    //File upload action
    app.post('/upload', fileHandler.handleUpload);

    app.post('/info', fileHandler.handleInfo);

    app.use(ErrorHandler);
}
