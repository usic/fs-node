FS-NODE
=======

File share service on node.js with mongodb


Installation
------------

1. Move to fs-node directory
2. Run `npm install`
3. Change config values in `config.json`
4. Run `node app.js`