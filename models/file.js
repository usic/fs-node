function FileModel(db){

	if ((this instanceof FileModel) === false) {
        return new FileModel(db);
	}

	var files = db.collection('files');

	this.insertFile = function(filename, user, tags, filesId, callback){
		
		var file = {
			'name': filename,
			'user': user,
			'tags': tags,
			'filesId': filesId, 
			'uploadTime': new Date(),
		};

		files.insert(file, {w: 1}, function(err, file){
			
			if(err) return callback(err, null);
			
			callback(err, file);
		
		});
	
	}

}

module.exports = FileModel;