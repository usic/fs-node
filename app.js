var express = require('express')
  , app = express() 
  , MongoClient = require('mongodb').MongoClient 
  , routes = require('./routes')
  , config = require('./config.json')
  , bodyParser = require('body-parser')
  , busboy = require('connect-busboy');

MongoClient.connect(config.dbpath, function(err, db) {

    if(err) throw err;

    app.use("/css", express.static(__dirname + '/views/css'));
    app.use("/js", express.static(__dirname + '/views/js'));
    app.use("/lib", express.static(__dirname + '/views/lib'));
    app.use("/images", express.static(__dirname + '/views/images'));

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded());

    app.use(busboy({limits: {fileSize: config.maxFileSize}}));

    // Application routes
    routes(app, db);

    app.listen(config.port);

    console.log('FS listening on port '+config.port);
});
